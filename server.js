
const express = require("express");
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var config = require("config");
app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.get("/",(req,res)=>{
    res.sendFile(__dirname+"/index.html");
});
port = process.env.PORT || 5000;
Message = mongoose.model('message',{
    name: String,
    message: String
})
var dbConfig = config.get('dbConfig.dblink');
mongoose.connect(dbConfig,{ useUnifiedTopology: true,useNewUrlParser: true  },(err)=>{
    if(err){
        console.log("not connected")
    }
    else{
        console.log("connected")
    }
})
io.on('connection',(socket)=>{
    console.log('user connected');
})
http.listen(port,()=>{
    console.log(`server listen on http://localhost:${port}`);
});
app.get("/messages",(req,res)=>{
   //! res.send("hello")
   Message.find({},(err,message)=>{
       res.send(message);
   })
})
app.post("/messages",(req,res)=>{
   var message = new Message(req.body);
   message.save((err,data)=>{
       if(err) {
           res.json({status:"message not saved"});
       }
       else {
           io.emit('message',req.body);
           res.json({status:"message saved"})
       }
       
})})

